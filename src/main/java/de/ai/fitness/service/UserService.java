package de.ai.fitness.service;

import de.ai.fitness.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    List<User> findAllUsers();
    Optional<User> findUserById(String id);
    User createUser(User user);
    User updateUserById(String id, User user);
    void deleteUserById(String id);
    void deleteAllUser();

}
