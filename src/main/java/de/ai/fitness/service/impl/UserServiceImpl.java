package de.ai.fitness.service.impl;

import de.ai.fitness.model.User;
import de.ai.fitness.repository.UserRepository;
import de.ai.fitness.service.UserInputValidationService;
import de.ai.fitness.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserInputValidationService userInputValidationService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserInputValidationService userInputValidationService) {
        this.userRepository = userRepository;
        this.userInputValidationService = userInputValidationService;
    }

    @Override
    public List<User> findAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public Optional<User> findUserById(String id) {
        return userRepository.findById(id);
    }

    @Override
    public User createUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public User updateUserById(String id, User user) {
        //TODO: solve this more elegantly. Exception handling not good.
        Optional<User> userToUpdate = userRepository.findById(id);
        userToUpdate.ifPresent(
                updatedUser -> {
                    updatedUser.setName(user.getName());
                    updatedUser.setLastName(user.getLastName());
                    updatedUser.setBirthday(user.getBirthday());
                    updatedUser.setWeight(user.getWeight());
                    updatedUser.setHeight(user.getHeight());
                    updatedUser.setCal(user.getCal());
                }
        );

        return userRepository.save(userToUpdate.get());
    }

    @Override
    public void deleteUserById(String id) {
        User userToDelete = findUserById(id).get();
        userRepository.deleteById(userToDelete.getId());
    }

    @Override
    public void deleteAllUser() {
        userRepository.deleteAll();
    }

}
