package de.ai.fitness.service.impl;

import de.ai.fitness.service.UserInputValidationService;
import org.springframework.stereotype.Service;

@Service
public class UserInputValidationServiceImpl implements UserInputValidationService {

    @Override
    public void validateId(String id) {
        if (!isValid(id)) {
            logAndThrowException(new IllegalArgumentException("Id must not be null!"));
        }
    }

    private boolean isValid(String userId) {
        return userId != null;
    }

    private void logAndThrowException(IllegalArgumentException e) {
        throw e;
    }
}
