package de.ai.fitness.model;


import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Comparator;
import java.util.Date;

@Document(collection = "user")
public class User implements Comparable<User> {

    private static final Comparator<User> COMPARATOR = Comparator.
            comparing((User user) -> user.name)
            .thenComparing(user -> user.lastName)
            .thenComparing(user -> user.birthday)
            .thenComparingDouble(user -> user.weight)
            .thenComparingDouble(user -> user.height)
            .thenComparingDouble(user -> user.cal);


    @Id
    private String id;

    private String name;
    private String lastName;
    private Date birthday;
    private double weight;
    private double height;
    private double cal;
    private int hashCode;

    public User() {
    }

    public User(String id, String name, String lastName, Date birthday, double weight, double height, double cal) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.birthday = birthday;
        this.weight = weight;
        this.height = height;
        this.cal = cal;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return  Double.compare(user.weight, weight) == 0 &&
                Double.compare(user.height, height) == 0 &&
                Double.compare(user.cal, cal) == 0 &&
                Objects.equal(id, user.id) &&
                Objects.equal(name, user.name) &&
                Objects.equal(lastName, user.lastName) &&
                Objects.equal(birthday, user.birthday);
    }

    @Override
    public int hashCode() {
        int result = hashCode;
        if (result == 0) {
            result = Objects.hashCode(id, name, lastName, birthday, weight, height, cal);
        }
        this.hashCode = result;
        return result;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("lastName", lastName)
                .add("birthday", birthday)
                .add("weight", weight)
                .add("height", height)
                .add("cal", cal)
                .add("hashCode", hashCode)
                .toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getCal() {
        return cal;
    }

    public void setCal(double cal) {
        this.cal = cal;
    }


    @Override
    public int compareTo(User o) {
        return COMPARATOR.compare(this, o);
    }
}
