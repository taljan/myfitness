package de.ai.fitness;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AifitnessApplication {

	public static void main(String[] args) {
		SpringApplication.run(AifitnessApplication.class, args);
	}

}
