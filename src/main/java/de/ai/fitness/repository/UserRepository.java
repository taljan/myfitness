package de.ai.fitness.repository;

import de.ai.fitness.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {
}
